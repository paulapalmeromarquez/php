# README
---
## Manual MarkDown
---
### Nivel 1

Este es un manual sobre sintaxis Mark Down, al contrario de los editores como LibreOffice que son WISIWNYG

### Negrita

Esto es una **negrita**.

### Cursiva

Esto es _cursiva_.

### Listas

**Listas**

* Elemento.
* Elemento
* Elemento

**Lista numerada**
1. Uno
2. dos
3. tres

### Código multinea

```php
<?php
echo "Hola Mundo">
?>
```

Código en una sola línea es así `echo "Hola Mundo"`

Un enlace se hace así [Texto de enlace] (https://trello.com/c/ONgEAV3l/22-git-b%C3%A1sico)


![Texto alt] (img/imagenprueba.jpg)


> Esto es una cita de Marx